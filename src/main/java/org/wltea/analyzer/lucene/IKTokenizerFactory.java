package org.wltea.analyzer.lucene;

import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.util.TokenizerFactory;
import org.apache.lucene.util.AttributeFactory;

import java.util.Map;

/**
 * Created by WWF
 */
public class IKTokenizerFactory extends TokenizerFactory {
    private boolean useSmart;

    public IKTokenizerFactory(Map<String, String> args) {
        super(args);
        useSmart = getBoolean(args, "useSmart", true);
    }

    @Override
    public Tokenizer create(AttributeFactory factory) {
        return new IKTokenizer(useSmart);
    }
}
